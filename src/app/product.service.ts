import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor() {}

  // create products
  products = [
    {
      name: 'product 1',
      price: '12000',
      author: 'TTB',
    },
    {
      name: 'product 2',
      price: '15000',
      author: 'bent',
    },
    {
      name: 'product 3',
      price: '10000',
      author: 'thanhbinh',
    },
    {
      name: 'product 4',
      price: '12000',
      author: 'will',
    },
    {
      name: 'product 5',
      price: '9000',
      author: 'victor',
    },
  ];

  getProduct() {
    // return the list of products
    return this.products;
  }
}
