import { Component } from '@angular/core';
import { ProductService } from './../product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.scss',
})
export class ProductComponent {
  products: any;
  constructor(private productService: ProductService) {
    this.products = productService.getProduct();
  }
}
